<?php

namespace PhpLowCodeEngine\OpenaiBundle\Services\Provider;

use App\Entity\Intergration;
use App\Services\Provider\ProviderInterface;
use App\Validator\Constraints\ProviderCreds;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @property string $key // Api key openai
 */
class OpenaiProvider implements ProviderInterface
{

    public function configureFields(Intergration $integration): iterable
    {
        yield TextField::new('key', 'Key')->setValue($integration->getCreds()['key'] ?? null);
    }

    static public function getName(): string
    {
        return 'Openai';
    }

    public function validateCreds(array $creds, ProviderCreds $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateCreds() method.
    }
}
