<?php

namespace PhpLowCodeEngine\OpenaiBundle\Services\NodeType;

use App\Entity\Intergration;
use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Execution\ExecutionData;
use App\Services\Store\StoreInterface;
use App\Services\Template\TemplateServiceInterface;
use App\Services\Trigger\Event\WorkflowCangeStateEvent;
use App\Validator\Constraints\NodeParams;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use OpenAI\Client;
use PhpLowCodeEngine\OpenaiBundle\Field\Type\ContextMessagesType;
use PhpLowCodeEngine\OpenaiBundle\OpenaiException;
use PhpLowCodeEngine\OpenaiBundle\Services\Provider\OpenaiProvider;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\Cache\CacheInterface;

class ChatGptNode implements \App\Services\NodeType\Types\NodeTypeInterface, \App\Services\Execution\ExecutionNodeInterface
{
    const GPT_MODELS_CACHE_KEY = 'gpt-models';
    private ?Client $openai = null;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly CacheInterface $cache,
        private readonly TemplateServiceInterface $templateService,
        private readonly StoreInterface $store
    )
    {
    }

    static public function getName(): string
    {
        return 'Chat GPT';
    }

    public function configureFields(Node $node): iterable
    {
        $integrationClass = OpenaiProvider::class;
        $openaiIntegrations = $this->entityManager
            ->getRepository(Intergration::class)
            ->createQueryBuilder('i')
            ->select(['i.id', 'i.name'])
            ->where("i.type = '{$integrationClass}'")
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);

        $choices = [];
        foreach ($openaiIntegrations as $openaiIntegration) {
            $choices[$openaiIntegration['name']] = $openaiIntegration['id'];
        }
        yield ChoiceField::new('integration')->setChoices($choices)->setValue($node->getParam('integration') ?? null);

        if ($node->getParam('integration')) {
            $models = $this->getModels($this->getClient($node));
            yield ChoiceField::new('model')->setChoices($models)->setValue($node->getParam('model') ?? null);
        }

        yield TextField::new('messages_key')->setValue($node->getParam('messages_key') ?? null);;

        yield CollectionField::new('messages')
            ->setFormTypeOptions([
                'by_reference' => false,
                'entry_type' => ContextMessagesType::class,
                'allow_add' => true,
                'allow_delete' => true
            ])
            ->allowAdd()
            ->setEntryIsComplex(true)
            ->setValue($node->getParam('messages') ?? []);

        yield BooleanField::new('save_session')->setValue($node->getParam('save_session') ?? null);;
        yield TextField::new('session_key')->setValue($node->getParam('session_key') ?? null);;
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        if (!preg_match('/\{\{[\w.]+}}/', $params['messages_data'])) {
            $context->buildViolation("Messages data must contains link to data like '{{ ... }}'")
                ->addViolation();
        }
    }

    public function run(NodeExecution $nodeExecution): string
    {
        $node = $nodeExecution->getNode();
        $inputData = $nodeExecution->getInputData();
        $model = $node->getParam('model');

        $threadMessages = [];
        if ($node->getParam('save_session')) {
            $sessionKey = $this->templateService->render($node->getParam('session_key') ?? '', $inputData);
            $threadMessages = $this->store->get($sessionKey) ?? [];
        }

        $messagesKey = $this->templateService->render($node->getParam('messages_key') ?? '', $inputData);
        $messages = array_merge(
            $threadMessages,
            $inputData->get($messagesKey) ?? [],
            $this->renderMessages($node, $inputData)
        );

        $chatParams = [
            'model' => $model,
            'messages' => $messages,
        ];

        $this->logger->info('Request to Chat GPT', [
            'request' => $chatParams,
            'method' => __METHOD__
        ]);

        $chat = $this->getClient($node)->chat()->create($chatParams)->toArray();

        $this->logger->info('Response from Chat GPT', [
            'response' => $chat,
            'method' => __METHOD__
        ]);

        $nodeExecution->setOutputData($chat);

        $message = $chat['choices'][0]['message'] ?? null;
        if ($message) {
            $messages[] = $message;
        }

        if ($node->getParam('save_session')) {
            $this->store->set($sessionKey, $messages);
        }

        return self::DEFAULT_RESULT;
    }

    private function renderMessages(Node $node, ExecutionData $data): array
    {
        $messages = [];
        foreach ($node->getParam('messages') ?? [] as $message) {
            $messages[] = [
                'role' => $message['role'],
                'content' => $this->templateService->render($message['content'], $data)
            ];
        }

        return $messages;
    }

    private function getClient(Node $node): Client
    {
        if (!$this->openai) {
            if (!$integrationId = $node->getParam('integration')) {
                throw new OpenaiException('Not exists openai integration yet');
            }

            $this->logger->debug("Search integration node '{$integrationId}'", ['method' => __METHOD__]);

            /** @var Intergration|OpenaiProvider $integration */
            $integration = $this->entityManager->find(Intergration::class, $integrationId);

            if (!$integration) {
                throw new OpenaiException("Not found integration '{$integrationId}'");
            }

            $this->logger->debug("Create Openai by creds", ['creds' => $integration->getCreds(), 'method' => __METHOD__]);

            $this->openai = \OpenAI::client($integration->key);

        }

        return $this->openai;
    }

    private function getModels(Client $client):array
    {
        return $this->cache->get(self::GPT_MODELS_CACHE_KEY, function () use ($client)
        {
            $response = $client->models()->list();

            $models = [];

            foreach ($response->data as $result) {
                if (str_contains($result->id, 'gpt')) {
                    $models[$result->id] = $result->id;
                }
            }

            return $models;
        });

    }

    #[AsEventListener(WorkflowCangeStateEvent::class)]
    public function onChangeWorkflowState(WorkflowCangeStateEvent $workflowCangeStateEvent)
    {
        $this->logger->debug("Clear GPT models cache", [
            'key' => self::GPT_MODELS_CACHE_KEY,
            'workflow' => $workflowCangeStateEvent->getWorkflow()->getId(),
            'workflow_state' => $workflowCangeStateEvent->getWorkflow()->isActive(),
            'method' => __METHOD__
        ]);

        $this->cache->delete(self::GPT_MODELS_CACHE_KEY);
    }

}
