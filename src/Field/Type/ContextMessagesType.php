<?php

namespace PhpLowCodeEngine\OpenaiBundle\Field\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ContextMessagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options):void
    {

        $builder
            ->add('role',ChoiceType::class, [
                'choices' => [
                    'system' => 'system',
                    'assistant' => 'assistant',
                    'user' => 'user'
                ]
            ])
            ->add('content', TextareaType::class);

    }
}
